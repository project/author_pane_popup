-- Author Pane Popup --

-- SUMMARY --

Shows author information in a tooltip manner.Integrates with Stylish
jQuery Tooltips.You can use various qTip style instances for author pane popup.

-- INSTALLTION --

1) Copy author_pane_popupo directory to your modules directory.

2) Download qTip jQuery plugin and place it inside libraries folder.
 
   qTip jQery plugin js file should be available here:
   
   /libraries/jquery.qtip/jquery.qtip.min.js

3) Enable the module at module configuration page.

4) Configure Author Pane Popup in
   admin/config/author_pane_popup/settings page.

-- FEATURES --

1) Show content author information as a tooltip.

2) Integrates with Stylish jQuery Tooltips.

-- Dependencies --

1) Views

-- SUGGESTIONS/REQUIREMENTS --

Add you suggestions, issues or new requirements under issue queue.
https://www.drupal.org/project/issues/author_pane_popup
Glad to include more features in to the module :)
